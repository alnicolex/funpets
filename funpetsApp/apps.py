from django.apps import AppConfig


class FunpetsappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'funpetsApp'
